package com.bigfans.cartservice.listener;

import com.bigfans.cartservice.model.Cart;
import com.bigfans.cartservice.service.CartService;
import com.bigfans.cartservice.service.impl.RedisService;
import com.bigfans.framework.CurrentUser;
import com.bigfans.framework.SessionUserFactory;
import com.bigfans.framework.kafka.KafkaConsumerBean;
import com.bigfans.framework.kafka.KafkaListener;
import com.bigfans.model.event.user.UserLoginEvent;
import org.springframework.beans.factory.annotation.Autowired;

@KafkaConsumerBean
public class UserListener {

    @Autowired
    private CartService cartService;
    @Autowired
    private RedisService redisService;

    @KafkaListener
    public void onLogin(UserLoginEvent loginEvent){
        try {
            Cart cart = redisService.myCart(loginEvent.getTempToken());
            CurrentUser currentUser = SessionUserFactory.getCurrentUser(loginEvent.getToken(), "bigfans");
            cartService.mergeMyCart(currentUser.getUid() , cart);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
