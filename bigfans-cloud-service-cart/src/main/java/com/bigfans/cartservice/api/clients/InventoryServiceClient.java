package com.bigfans.cartservice.api.clients;

import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-16 下午3:06
 **/
@Component
public class InventoryServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Integer> getSurplusStock(String prodId) {
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://catalog-service/stock?prodId={prodId}").build().expand(prodId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse restResponse = responseEntity.getBody();
            Integer stock = (Integer) restResponse.getData();
            return stock;
        });
    }

}
