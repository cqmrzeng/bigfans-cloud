package com.bigfans.searchservice.api;

import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.searchservice.service.HotSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HotSearchApi extends BaseController {

    @Autowired
    private HotSearchService hotSearchService;

    @GetMapping("/hotSearch")
    public RestResponse hotSearch(){
        return RestResponse.ok(hotSearchService.hotSearch());
    }

}
