package com.bigfans.searchservice.service;

import com.bigfans.framework.model.FTSPageBean;
import com.bigfans.searchservice.model.Product;
import com.bigfans.searchservice.model.ProductSearchRequest;

public interface ProductSearchService {

	FTSPageBean<Product> searchProduct(ProductSearchRequest param , int from , int size) throws Exception;

}
