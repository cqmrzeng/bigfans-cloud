package com.bigfans.catalogservice.dao;

import com.bigfans.catalogservice.model.Brand;
import com.bigfans.framework.dao.BaseDAO;

import java.util.List;


public interface BrandDAO extends BaseDAO<Brand> {
	
	List<Brand> search(String keyword, String categoryId, Long start, Long pagesize);

}
