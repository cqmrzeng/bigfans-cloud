package com.bigfans.catalogservice.service.product;

import com.bigfans.catalogservice.model.Product;
import com.bigfans.catalogservice.model.ProductImage;
import com.bigfans.catalogservice.model.ProductSpec;
import com.bigfans.framework.dao.BaseService;
import com.bigfans.framework.plugins.UploadResult;

import java.io.InputStream;
import java.util.List;

public interface ProductService extends BaseService<Product>{
	
	List<Product> listByPgId(String pgId) throws Exception;
	
	List<Product> listByCategory(String categoryId , Long start , Long pagesize) throws Exception;
	
	List<Product> listByCategory(String[] categoryIdList , Long start , Long pagesize) throws Exception;
	
	List<Product> listHotSale(Long start , Long pagesize) throws Exception;
	
	void create(Product product, List<ProductImage> imgList, List<ProductSpec> specList) throws Exception;
	
	void updateHits(String productId) throws Exception;
	
	UploadResult uploadImage(InputStream is , String imageExtension) throws Exception;
	
	UploadResult removeImage(String fileKey) throws Exception;

	List<ProductSpec> listSpecsById(String pid) throws Exception;

	Product getDetailById(String pid) throws Exception;
	
}
