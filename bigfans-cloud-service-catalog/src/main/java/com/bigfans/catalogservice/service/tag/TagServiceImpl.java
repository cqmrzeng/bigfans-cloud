package com.bigfans.catalogservice.service.tag;

import com.bigfans.catalogservice.dao.TagDAO;
import com.bigfans.catalogservice.model.Tag;
import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.framework.event.ApplicationEventBus;
import com.bigfans.model.event.tag.TagCreatedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2016年2月29日 上午9:44:36 
 * @version V1.0
 */
@Service(TagServiceImpl.BEAN_NAME)
public class TagServiceImpl extends BaseServiceImpl<Tag> implements TagService {

	public static final String BEAN_NAME = "tagService";
	
	private TagDAO tagDAO;

	@Autowired
	private ApplicationEventBus applicationEventBus;
	
	@Autowired
	public TagServiceImpl(TagDAO tagDAO) {
		super(tagDAO);
		this.tagDAO = tagDAO;
	}

	@Override
	public void create(Tag e) throws Exception {
		super.create(e);
		applicationEventBus.publishEvent(new TagCreatedEvent(e.getId()));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Tag> listByProdId(String pid) throws Exception {
		return tagDAO.listByProdId(pid);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Tag> listDuplicate(String value) throws Exception {
		return tagDAO.listByContent(value);
	}
	
	@Override
	@Transactional
	public String mergeDuplicates(List<Tag> duplicates) throws Exception {
		
		return "";
	}
	
	@Override
	@Transactional
	public Integer increaseRelatedCount(String tagId, Integer increase) {
		// TODO Auto-generated method stub
		return null;
	}
}
