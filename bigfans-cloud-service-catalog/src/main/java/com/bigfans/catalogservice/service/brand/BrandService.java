package com.bigfans.catalogservice.service.brand;

import com.bigfans.catalogservice.model.Brand;
import com.bigfans.framework.dao.BaseService;
import com.bigfans.framework.plugins.UploadResult;

import java.io.InputStream;

public interface BrandService extends BaseService<Brand> {

	UploadResult uploadLogo(InputStream is, String imageExtension) throws Exception ;
	
	UploadResult deleteLogo(String imgPath) throws Exception;
}
