package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;

import javax.persistence.Column;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * 
 * @Description:商品属性,和分类是一对多的关系.
 * @author lichong 
 * 2015年8月9日下午7:36:28
 *
 */
@Table(name="AttributeOption")
public class AttributeOptionEntity extends AbstractModel {

	private static final long serialVersionUID = 8949896457611748295L;
	
	public static final String INPUTTYPE_SELECT = "S";
	public static final String INPUTTYPE_MANUL = "M";

	@Column(name="category_id")
	protected String categoryId;
	@Column(name="name")
	protected String name;
	@Column(name="code")
	protected String code;
	@OrderBy
	@Column(name="order_num")
	protected Integer orderNum;
	@Column(name="input_type")
	protected String inputType;
	@Column(name="searchable")
	protected Boolean searchable;
	@Column(name="required")
	protected Boolean required;

	@Override
	public String getModule() {
		return "AttributeOption";
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
	
	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public Boolean getSearchable() {
		return searchable;
	}

	public void setSearchable(Boolean searchable) {
		this.searchable = searchable;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}
}
