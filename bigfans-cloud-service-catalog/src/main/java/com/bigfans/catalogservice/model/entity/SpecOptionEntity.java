package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;


/**
 * 
 * @Description:商品规格项,定义在类别上面,创建商品组时可以选择规格项
 * @author lichong
 * 2015年8月9日下午7:51:58
 *
 */
@Data
@Table(name="SpecOption")
public class SpecOptionEntity extends AbstractModel {
	
	private static final long serialVersionUID = -477834366515589009L;
	
	public static final String INPUTTYPE_SELECT = "S";
	public static final String INPUTTYPE_MANUL = "M";

	public String getModule() {
		return "SpecOption";
	}
	
	@Column(name="name")
	protected String name;
	@Column(name="category_id")
	protected String categoryId;
	@Column(name="code")
	protected String code;
	/**
	 * M 手动输入
	 * S 从固定值中选
	 * B 布尔值
	 */
	@Column(name="input_type")
	protected String inputType;
	@Column(name="order_num")
	protected Integer orderNum;
	
}
