package com.bigfans.model.dto.order;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lichong
 * @create 2018-02-17 下午12:52
 **/
@Data
public class OrderPricingDto {
    private String userId;
    private List<OrderItemPricingDto> items = new ArrayList<>();
    private String usedCouponId;
    private Float usedPoints;

    public void addItem(OrderItemPricingDto itemCalculateDto){
        this.items.add(itemCalculateDto);
    }
}
