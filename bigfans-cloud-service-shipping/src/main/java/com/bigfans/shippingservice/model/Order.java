package com.bigfans.shippingservice.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Order {

    protected String id;
    protected Date createDate;
    protected Date updateDate;
    // 用户信息
    protected String userId;
    // 支付信息
    protected String paymentId;
    // 支付类型ID
    protected String payMethodCode;
    // 支付类型名称
    protected String payMethodName;
    // 收货地址
    protected String addressId;
    // 收货人
    protected String addressConsignee;
    // 送货详细地址
    protected String addressDetail;
    // 收货人电话
    protected String addressPhone;
    // 收货人email
    protected String addressEmail;
    // 留言
    protected String note;
    // 运费
    protected BigDecimal freight;
    protected Integer prodTotalQuantity;
    // 商品总价格
    protected BigDecimal prodTotalPrice;
    // 总价格(计算完邮费和各种优惠后的应付款额)
    protected BigDecimal totalPrice;
}
