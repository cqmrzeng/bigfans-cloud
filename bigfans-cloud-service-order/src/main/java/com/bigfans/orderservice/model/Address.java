package com.bigfans.orderservice.model;

import lombok.Data;

/**
 * @author lichong
 * @create 2018-03-14 下午10:14
 **/
@Data
public class Address {

    protected String userId;
    /* 收货人姓名 */
    protected String consignee;
    /* 电子邮箱 */
    protected String email;
    /* 邮编 */
    protected String postalcode;
    /* 座机 */
    protected String tel;
    /* 手机 */
    protected String phone;
    protected String address;
}
