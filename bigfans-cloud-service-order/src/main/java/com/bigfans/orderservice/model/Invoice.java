package com.bigfans.orderservice.model;

import com.bigfans.orderservice.model.entity.InvoiceEntity;
import lombok.Data;

/**
 * 发票
 * @author lichong
 *
 */
@Data
public class Invoice extends InvoiceEntity {

	private static final long serialVersionUID = 8131690523865755118L;

}
