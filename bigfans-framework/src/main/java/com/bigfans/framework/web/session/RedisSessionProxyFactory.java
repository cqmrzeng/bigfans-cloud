package com.bigfans.framework.web.session;

import java.lang.reflect.Proxy;

import javax.servlet.http.HttpSession;

/**
 * 
 * @Description:session的代理类工厂
 * @author lichong
 * @date Feb 2, 2016 8:03:26 PM
 *
 */
public class RedisSessionProxyFactory {

	public static HttpSession wrap(HttpSession httpSession) {
		if (httpSession == null) {
			return null;
		}
		return (HttpSession) Proxy.newProxyInstance(RedisSessionProxyFactory.class.getClassLoader(),
				new Class[] { HttpSession.class }, new RedisSession(httpSession));
	}

}
