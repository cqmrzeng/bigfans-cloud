package com.bigfans.framework.neo4j;

import org.neo4j.driver.v1.Record;

public interface Neo4jRowMapper<T> {

    T map(Record record);

}
