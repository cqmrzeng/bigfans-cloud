package com.bigfans.framework.neo4j;

import java.util.concurrent.TimeUnit;

import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Config;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;

public class Neo4jConnectionFactory {

	private String uri;
	private String username;
	private String password;
	
	private Driver driver;
	
	public void build() {
		if (driver == null) {
			driver = GraphDatabase.driver(uri, AuthTokens.basic(username, password),
					Config.build()
	                .withMaxTransactionRetryTime(15, TimeUnit.SECONDS)
	                .withMaxIdleSessions(100)
	                .toConfig());
		}
	}
	
	public Driver getDriver() {
		return driver;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
