package com.bigfans.framework.plugins;


/**
 *
 * @Description: 插件接口
 * @author lichong
 * @date Jan 29, 2016 4:46:10 PM 
 *
 */
public interface Plugin {
	
	void plugin();
	
	void unplugin();

}
