package com.bigfans.framework.plugins;

import java.io.File;
import java.io.InputStream;

/**
 * 
 * @Title:
 * @Description: 文件上传插件
 * @author lichong
 * @date 2015年12月23日 上午9:27:05
 * @version V1.0
 */
public interface FileStoragePlugin extends Plugin {
	
	UploadResult upload(File srcFile, String target);

	UploadResult uploadQrCodeImg(File srcFile, String orderNo);

	UploadResult uploadProductImg(File srcFile, String prodId);
	
	UploadResult uploadFile(InputStream is ,String target);
	
	UploadResult deleteFile(String target);
	
	UploadResult listFiles(String path);
	
	File download(String target);
	
	String getStorageType();

	boolean fileExists(String path);

}
